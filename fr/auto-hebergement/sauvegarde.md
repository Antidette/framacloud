Serveur de sauvegarde
=====================

Il y a deux types de personnes : celles qui sauvegardent régulièrement
leurs données et celles qui n’ont pas encore perdu tous leurs documents.

Vous pouvez utiliser votre serveur pour sauvegarder vos documents
régulièrement. D’ailleurs, je vous le conseille vivement!

Quelques pré-requis
-------------------

Afin de mettre en place notre serveur de sauvegarde, il est nécessaire
d’avoir installé dessus le paquet `rsync`.

Il vous faudra aussi un accès via ssh (voir [Préparer l’accès ssh](installation.html#pr%C3%A9parer-lacc%C3%A8s-ssh)) et pouvoir vous
connecter via un jeu de clés afin de ne pas avoir à entrer de mot de
passe (voir [Connexion automatique à l'aide de clé](installation.html#connexion-automatique-%C3%A0-laide-de-cl%C3%A9s)).

On supposera dans la suite que vous pouvez vous connecter via ssh sur
votre serveur avec la commande suivante :

```
ssh backupsrv
```

Enfin, les sauvegardes seront placées dans le dossier `/media/backup`
sur le serveur. À vous d’adapter la suite en fonction de votre cas.

<p class="alert alert-info">rsync est un programme très malin, qui est capable de
transférer seulement les parties qui ont changé sur votre disque.</p>

Faire une sauvegarde
--------------------

Pour les sauvegardes, on va lancer certaines commandes à partir des
machines à sauvegarder qui enverront vos documents sur le serveur.

Pour cela, on utilise *rsync* ainsi :

```
rsync -e "ssh" -rtuz --del dossier-à-sauver/ backupsrv:/media/backup/
```

**Attention** : *le “/” est important à la fin d’un dossier*

Bien entendu, modifiez `dossier-à-sauver` par le chemin du dossier
contenant ce que vous souhaitez sauvegarder.

<div class="alert alert-warning">
    <p>Quelques explications s’imposent : </p>
    <ul>
        <li><code>-e "ssh"</code> : permet d’utiliser un transfert ssh</li>
        <li><code>-rt</code> : La sauvegarde est récursive (on va dans les
            sous-dossiers) et se souvient de la date de modification des
            fichiers.</li>
        <li><code>-u</code> : Ne transfère que ce qui a été modifié depuis
            la dernière sauvegarde. C’est donc plus rapide.</li>
        <li><code>-z</code> : Compresse les données lors du transfert</li>
        <li><code>--del</code> : Supprime les documents ayant disparus sur
            le dossier source</li>
        <li><code>backupsrv</code> : C’est le raccourci vers le serveur ssh
            définit au chapitre <a href="installation.html#connexion-automatique-%C3%A0-laide-de-cl%C3%A9s">Connexion automatique à l’aide de clé</a>. Sans cela, cette partie serait
            beaucoup plus longue :
       <pre>rsync -e "ssh" -rtuz --del dossier-à-sauver/ utilisateur@serveurssh.com:/media/backup</pre></li>
    </ul>
</div>

Sauvegarder régulièrement
-------------------------

Pour que les sauvegardes se fassent automatiquement et régulièrement, on
peut utiliser une tâche cron.

<div class="alert alert-info">
    <p>cron c’est le gentil démon qui s’occupe de surveiller les
    choses à exécuter régulièrement. Par exemple archiver les messages
    système (logs) tous les jours.</p>
    <p>Pour être sûr de l’avoir, installer le paquet <code>anacron</code>.</p>
</div>

Pour ajouter la sauvegarde, tapez `crontab -e`, puis entrez ceci :

```
@weekly /usr/bin/rsync -e "ssh" -rtuz --del \
    dossier-à-sauver/ backupsrv:/media/backup/
```

Enregistrez et quittez, ça y est, la sauvegarde se fera toutes les
semaines avec l’exemple ci-dessus.

Vous pouvez choisir à quel moment sera lancée la sauvegarde en modifiant
ce qui est mis en début de ligne à la place du `@weekly`. Voici quelques
exemples courants :

-   `@daily` : Tous les jours
-   `@hourly` : Chaque heure
-   `0 9 * * *` : Tous les jours à 9 heures.


