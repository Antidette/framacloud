Références
==========

Voici quelques références ayant servi à la rédaction de ce document,
ainsi que des liens permettant de compléter ce qui a été écrit ici.

-   [Le wiki sur l’auto-hébergement](http://www.auto-hebergement.fr/)
-   [Un annuaire d’applications et de services à auto-héberger](http://waah.quent1.fr/)
-   [Une distribution pour l’auto-hébergement](https://yunohost.org)
-   [calomel.org](https://calomel.org) propose divers tutoriels de
    qualité
-   [icecast](http://icecast.org/) est un serveur
    de streaming
-   [idjc est un logiciel pour faire des podcasts](http://idjc.sourceforge.net/)
-   [serveur de sauvegarde –
    coagul](http://coagul.org/drupal/publication/installer-serveur-sauvegardes-sous-linux)
-   [Cultiver son jardin de
    Framasoft](https://framacloud.org/fr/cultiver-son-jardin/), plusieurs
    tutoriels détaillés décrivant l’installation de services webs.
-   Applications faciles à installer et à auto-héberger choisies par
    sebsauvage : <http://sebsauvage.net/auto/>
