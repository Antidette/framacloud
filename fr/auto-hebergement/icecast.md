<span>m<span>0.7</span> m<span>0.3</span></span> Vous pouvez diffuser de
la musique ou des vidéos avec icecast. Si si! De quoi mettre en place
votre propre radio ou encore héberger un podcast. &
![image](images/podcastlogo)\

Comment ça marche? En gros, vous envoyez vos musiques en direct sur le
serveur, ou même votre voix, et lui il s’occupe de le rendre accessible
sur le réseau.

### Installation d’icecast

Hop, on passe comme d’habitude par apt :

``` {frame="leftline"}
    # apt-get install icecast2
```

Lors de l’installation, il est possible que vous ayez à répondre à
quelques questions. Répondez que oui, vous le voulez, ou sinon, passez
directement à la partie suivante [configicecast].

Dans le cas où vous avez répondu “Oui” :

-   Le nom d’hôte : c’est le nom de domaine de votre serveur. Par
    exemple, “iloverocknroll.net”. Si vous souhaitez utiliser icecast
    sur un réseau local, laissez “localhost”.

-   Le mot de passe pour la source : c’est le mot de passe qui servira à
    envoyer du son sur le serveur, qui le redistribuera.

-   Mot de passe pour le relai et pour l’administration : le mot de
    passe qu’il faudra utiliser pour relayer le flux ou pour administrer
    icecast.

Si vous utilisez icecast pour diffuser sur internet et non sur un réseau
local, pensez à rediriger dans votre box[^1] le port 8000 (TCP) vers
votre serveur et aussi l’ouvrir dans le pare-feu.

### Configuration manuelle de icecast {#configicecast}

Votre configuration doit maintenant se retrouver dans le fichier
`/etc/icecast2/icecast.xml`. Les lignes importantes à modifier sont
toutes celles contenant “hackme” ci-dessous:

``` {frame="leftline"}
    <authentication>
        <!-- Sources og in with username 'source' -->
        <source-password>hackme</source-password>
        <!-- Relays log in username 'relay' -->
        <relay-password>hackme</relay-password>

        <!-- Admin logs in with the username given below -->
        <admin-user>admin</admin-user>
        <admin-password>hackme</admin-password>
    </authentication>

    <hostname>localhost(hackme)</hostname>
```

### Activer icecast

Pour activer icecast, modifiez le fichier `/etc/default/icecast` pour
avoir à la fin la ligne suivante :

``` {frame="leftline"}
    ENABLE=true
```

Recharger ensuite icecast avec la commande

``` {frame="leftline"}
    service icecast2 restart
```

### Accéder à l’interface

Dans un navigateur, ouvrez l’adresse http://localhost:8000 ou bien
http://votredomaine.com:8000 pour admirer l’interface encore vide de
icecast :

![image](images/icecast)

### Diffuser un flux

#### Avec vlc : 

La solution de facilité, c’est avec vlc.

Une fois vlc ouvert, cliquez sur “Media” -\> “Flux”. Ajoutez un fichier
puis cliquez sur diffuser.

![image](images/vlc_icecast)

Cliquez sur “Next”, puis choisissez dans “Nouvelle destination”
“IceCast”. Cliquez sur “Ajouter”.

![image](images/vlc_icecast2)

Remplissez ensuite les différents champs :

-   Adresse : localhost ou bien votredomaine.com

-   Port utilisé : 8000 par défaut

-   Point de montage : le nom de votre flux

-   Utilisateur:mot de passe : |source:votremotdepasse|

L’utilisateur ici est “source”, à moins d’avoir modifié le fichier de
configuration sur ce point.

![image](images/vlc_icecast3)

Vous pouvez activer le transcodage si vous le souhaitez. Audio Vorbis
(OGG) fonctionne bien en général. Il vous reste à valider la suite.

Vous pouvez alors aller dans votre navigateur à l’interface de icecast
pour découvrir votre flux :

![image](images/icecast_web)

En cliquant sur “M3u”, vous pouvez écouter votre flux! Donnez cette
adresse à n’importe qui pour qu’il vous écoute.

#### Diffuser avec ices2 : 

Ices2 (paquet du même nom à installer) s’utilise pour sa part en ligne
de commande. Debian fournit des exemples de configuration dans
`/usr/share/doc/ices2/examples`.

Copiez par exemple `/usr/share/doc/ices2/examples/ices2-alsa.xml` et
éditez les champs à l’intérieur selon votre configuration, pour obtenir
par exemple :

``` {frame="leftline"}
<?xml version="1.0"?>
<ices>

    <!-- run in background  -->
    <background>0</background>
    <!-- where logs go. -->
    <logpath>/tmp/</logpath>
    <logfile>ices.log</logfile>
    <!-- size in kilobytes -->
    <logsize>2048</logsize>
    <!-- 1=error, 2=warn, 3=infoa ,4=debug -->
    <loglevel>4</loglevel>
    <!-- logfile is ignored if this is set to 1 -->
    <consolelog>1</consolelog>

    <!-- optional filename to write process id to -->
    <!-- <pidfile>/home/ices/ices.pid</pidfile> -->

    <stream>
        <!-- metadata used for stream listing -->
        <metadata>
            <name>My amazing stream</name>
            <genre>Variable</genre>
            <description>Un peu de tout et n'importe quoi</description>
            <url>http://votredomaine.net</url>
        </metadata>

        <!--    Input module.

            This example uses the 'alsa' module. It takes input from the
            ALSA audio device (e.g. line-in), and processes it for live
            encoding.  -->
        <input>
            <module>alsa</module>
            <param name="rate">44100</param>
            <param name="channels">2</param>
            <param name="device">pulse</param>
            <!-- Read metadata (from stdin by default, or -->
            <!-- filename defined below (if the latter, only on SIGUSR1) -->
            <param name="metadata">1</param>
            <param name="metadatafilename">test</param>
        </input>

        <!--    Stream instance.

            You may have one or more instances here.  This allows you to
            send the same input data to one or more servers (or to different
            mountpoints on the same server). Each of them can have different
            parameters. This is primarily useful for a) relaying to multiple
            independent servers, and b) encoding/reencoding to multiple
            bitrates.

            If one instance fails (for example, the associated server goes
            down, etc), the others will continue to function correctly.
            This example defines a single instance doing live encoding at
            low bitrate.  -->

        <instance>
            <!--    Server details.

                You define hostname and port for the server here, along
                with the source password and mountpoint.  -->

            <hostname>votredomaine.net</hostname>
            <port>8000</port>
            <password>c'estunsecret</password>
            <mount>/Thubanstream.ogg</mount>
            <yp>0</yp>   <!-- allow stream to be advertised on YP, default 0 -->

            <!--    Live encoding/reencoding:

                channels and samplerate currently MUST match the channels
                and samplerate given in the parameters to the alsa input
                module above or the remsaple/downmix section below.  -->

            <encode>  
                <quality>0</quality>
                <samplerate>22050</samplerate>
                <channels>1</channels>
            </encode>

            <!-- stereo->mono downmixing, enabled by setting this to 1 -->
            <downmix>1</downmix>

            <!-- resampling.

                Set to the frequency (in Hz) you wish to resample to, -->

            <resample>
                <in-rate>44100</in-rate>
                <out-rate>22050</out-rate>
            </resample>
        </instance>

    </stream>
</ices>
```

Modifiez la partie |\<param name=“device”\>pulse\</param\>| si vous
voulez diffuser ce qu’entend votre micro. (par exemple |\<param
name=“device”\>hw:0,0\</param\>|).

Enfin, pour diffuser avec ices2, il faut juste lancer cette commande:

``` {frame="leftline"}
    ices2 fichier_de_config.xml
```

### Créer son podcast

Un logiciel incontournable sur linux pour créer votre podcast est idjc.
Il vous permettra de définir une liste de lecture, des sons à jouer
ponctuellement (type jingle), d’enregistrer vos émissions, de mixer des
appels VoIP avec votre flux. Bref, tout y est pour faire un podcast. Par
contre, il peut paraître déroutant au premier abord.

![image](images/idjc)

Vous disposez de 2 panneaux, dans lesquels vous pouvez ajouter des
musiques à jouer (via un clic-droit). En dessous des panneaux, vous avez
deux boutons : “Flux” et “DJ”. Si “Flux” est appuyé, cela envoie au
serveur icecast la musique jouée. Si “DJ” est activé, vous pouvez
entendre ce qui est envoyé comme flux.

Une barre de “crossfade” permet de passer d’une playlist à une autre.

À droite, vous avez tous les niveaux sonores.

En haut, il y a un onglet “Effet”, qui permet de définir des jingles.

Bon, ce n’est pas tout ça, mais il faut se connecter à notre serveur
IceCast. Pour cela, cliquez sur le menu “Voir” $\rightarrow$ “Sortie”.
Une nouvelle fenêtre apparaît.

![image](images/idjc_config1)

Dans l’onglet 1, cliquez sur “Configuration”, puis sur le bouton
“Nouveau”, afin de définir les options pour se connecter au serveur.

![image](images/idjc_config2)

Choisissez ensuite l’onglet “format”. Choisissez le paramètre “Family”,
par exemple Xiph/Ogg, puis cliquez sur la flèche droite de façon à
définir les autres paramètres.

![image](images/idjc_config3)

Enfin, vous pouvez compléter les derniers paramètres des autres onglets.

Déroulez maintenant “Contrôles individuels”. Vous pouvez cocher
éventuellement “Démarrer le lecteur” et “Démarrer l’enregistreur” à la
connexion. Il vous reste à cliquer sur le bouton
“votreserveur.com/listen”.

![image](images/idjc_config4)

Si quelqu’un vous écoute, vous verrez dans la fenêtre principale en
dessous du casque à droite (à côté des niveaux sonores) le nombre
d’auditeurs.

Amusez-vous bien! $\smiley$

[^1]: voir [portredirect]

