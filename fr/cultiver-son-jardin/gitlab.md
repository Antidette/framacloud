# Installation de Gitlab et Mattermost

## Introduction

On ne présente plus [Gitlab][1] aux barbus qui font du développement...
l'alternative la plus populaire à github pour gérer le versionnement de
son code via un site web, que nous utilisons pour… [Framagit][2] (surprise !).

![](images/gitlab/framagit-screenshot.png)

[Mattermost][3] est un logiciel de chat en groupe, alternative au service
privateur Slack. Voyez-le comme une alternative plus moderne à IRC :
on peut y créer des groupes de discussion facilement, en y incluant des médias.
Du coup, nous nous appuyons sur celui-ci pour offrir [Framateam][4].

![](images/gitlab/mattermost-framateam-screen.png)

[Depuis Août 2015 et la version 7.22][5], Gitlab intègre directement
Mattermost dans ses versions. Nous allons pouvoir faire d'une pierre deux coups !

<div class="alert alert-info">
  Si Gitlab et Mattermost vont désormais de pair, il est toujours possible
  d'installer Mattermost tout seul, en <em>stand alone</em> comme on dit dans le jargon.
  N'hésitez pas à jeter un oeil à la
  <a href="http://docs.mattermost.com/install/requirements.html#">documentation de Mattermost</a> pour cela !
</div>

## Prérequis

La machine utilisée pour ce tutoriel tourne sous Linux Debian Jessie.
C'est tout ! Pas de serveur web ni de base de données, Gitlab embarque
ce dont il a besoin avec son package.

<div class="alert alert-info">
  La version de Gitlab installée ici est la version omnibus (c'est à dire
  qu'elle inclut tous les packages dont elle pourrait avoir besoin),
  d'où le peu de dépendances.
</div>

En option, vous pourriez avoir besoin :

*   d’un nom de domaine correctement configuré pour rediriger vers votre serveur
*   d’un serveur mail opérationnel pour les notifications de messages

<div class="alert alert-info">
  Prenez aussi le temps de regarder les <a href="http://docs.gitlab.com/ce/install/requirements.html">prérequis hardware de gitlab</a> :
  c'est un logiciel qui peut être un peu gourmand (selon la volumétrie
  d'utilisateurs que vous aurez), ils recommandent un minimum de deux cœurs et 2 Go de mémoire vive.
  Il est possible mais pas vraiment conseillé de l'installer sur une
  « petite » machine comme un Raspberry Pi pour une utilisation au quotidien ! ;-)
</div>

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Commençons par installer l'indispensable (certains packages devraient
être installés par défaut, mais rien ne coûte de vérifier) :

    sudo apt-get install curl openssh-server ca-certificates

Ensuite, configuration des sources :

    curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh > script.deb.sh

Ne pas hésiter à ouvrir le fichier `script.deb.sh` pour savoir ce qu'il
fait (en plus il est bien commenté !)

    sudo bash script.deb.sh

Vous devriez avoir une réponse de ce genre:

    Detected operating system as debian/jessie.
    Checking for curl...
    Detected curl...
    Running apt-get update... done.
    Installing debian-archive-keyring which is needed for installing
    apt-transport-https on many Debian systems.
    Installing apt-transport-https... done.
    Installing /etc/apt/sources.list.d/gitlab_gitlab-ce.list...done.
    Importing packagecloud gpg key... done.
    Running apt-get update... done.

    The repository is setup! You can now install packages.

### 2 - Semer

![](images/icons/semer.png)

Passons aux choses sérieuses avec l'installation :

    sudo apt-get install gitlab-ce

L'installation nous conseillera de lancer un `sudo gitlab-ctl reconfigure`
mais faisons quelques ajustements avant cela, que ce soit pour Gitlab
ou pour Mattermost.

#### Configuration de Gitlab

    sudo vi /etc/gitlab/gitlab.rb

Les options à contrôler/décommenter sont les suivantes dans notre cas:

*   `external_url 'gitlab.example.org'` : l'URL du gitlab (vous noterez
    qu'il n'y a pas de '=' au contraire des autres paramètres)
*   `gitlab_rails['gitlab_email_enabled']` : pour les mails de notification
*   `gitlab_rails['gitlab_email_from']` : l'adresse de provenance des mails
*   `gitlab_rails['gitlab_email_display_name']` : un petit nom sympa "Gitlab de XXX"
*   `gitlab_rails['gitlab_email_reply_to']` : une adresse de réponse
    pour les mails (champ *reply to*: nous avons mis une adresse noreply
    ici car nous ne gérons pas les réponses par mail)
*   `gitlab_rails['gitlab_default_theme']` : choisissez ici le thème par
    défaut. Framasoft a fait le choix 1 (contre 2 par défaut)
*   `gitlab_rails['gitlab_default_projects_features_merge_requests']` :
    pour activer (ou non) par défaut les *merge request* par défaut sur les projets
*   `gitlab_rails['gitlab_default_projects_features_wiki']` : pour activer
    (ou non) par défaut les wikis
*   `gitlab_rails['gitlab_default_projects_features_snippets']` : pour
    activer (ou non) par défaut les snippets (comme les gists de github)
*   `gitlab_rails['incoming_email_enabled']` : permettre aux utilisateurs
    de répondre aux tickets et *merge requests* par mail.
    Framasoft l'a désactivé. [Une documentation ici][6] pour le mettre en place
*   Authentification LDAP, pour utiliser un LDAP pour se loguer :

        gitlab_rails['ldap_enabled'] = true
        gitlab_rails['ldap_servers'] = YAML.load <<-'EOS' # remember to close this block with 'EOS' below
           main: # 'main' is the GitLab 'provider ID' of this LDAP server
             label: 'LDAP'
             host: 'ldap.example.org'
             port: 636
             uid: 'uid'
             method: 'ssl' # "tls" or "ssl" or "plain"
             bind_dn: 'uid=ldapaccess,ou=users,dc=example,dc=org'
             password: 'password'
             active_directory: false
             allow_username_or_email_login: true
             block_auto_created_users: false
             base: 'ou=users,dc=example,dc=org'
             user_filter: '(uid!=ldapaccess)'
             attributes:
               username: ['uid', 'userid', 'sAMAccountName']
               email:    ['mail', 'email', 'userPrincipalName']
               name:       'cn'
               first_name: 'givenName'
               last_name:  'sn'
        EOS

*   Permettre l'authentification via Oauth (créer des "applications"
    sur github et gitlab pour avoir les `app_id` et `app_secret`) :

        gitlab_rails['omniauth_enabled'] = true
        gitlab_rails['omniauth_allow_single_sign_on'] = true  (**['saml']** ???)
        gitlab_rails['omniauth_block_auto_created_users'] = false
        gitlab_rails['omniauth_auto_link_ldap_user'] = false
        gitlab_rails['omniauth_providers'] = [
           {
             "name" => "github",
             "label" => "GitHub",
             "app_id" => "",
             "app_secret" => "",
             "args" => { "scope" => "user:email" }
           },{
             "name" => "gitlab",
             "label" => "GitLab.com",
             "app_id" => "",
             "app_secret" => "",
             "args" => { "scope" => "api" }
           }
        ]

*   `gitlab_rails['backup_keep_time']` : durée en secondes de rétention
    des sauvegardes que Gitlab fait tous les jours (dépôts + dump SQL)
    Attention, cela peut vite prendre de la place ! Comme c'est notre
    cas chez Framasoft, nous avons un montage ssh sur `/opt/backup`,
    vers lequel nous envoyons ces sauvegardes une fois qu'elles sont faites :

        gitlab_rails['backup_upload_connection'] = {
          :provider => 'Local',
          :local_root => '/opt/backup/'
        }
        gitlab_rails['backup_upload_remote_directory'] = '.' # ou '/gitlab' si vous voulez les mettre dans '/opt/backup/gitlab'

Cependant, il faut encore supprimer les anciennes sauvegardes régulièrement.
Nous faisons ça avec un cron (suppression des sauvegardes qui datent de
plus de 15 jours) : `17 8 * * * find /opt/backup -mtime "+15" -delete`

*   Pour finir :

    *   `gitlab_rails['extra_piwik_url'] = 'piwik.example.org'` : pour
        les stats piwik, l'hôte du piwik
    *   `gitlab_rails['extra_piwik_site_id']` : l'id piwik de votre gitlab
    *   `nginx['redirect_http_to_https']` : rediriger toutes les requêtes
        http vers la version https
    *   `nginx['ssl_certificate']` : le chemin absolu du certificat
        (avec sa chaîne de certification)
    *   `nginx['ssl_certificate_key']` : le chemin absolu de la clé du
        certificat
    *   `nginx['ssl_dhparam']` : il est toujours bon pour la sécurité de
        générer un fichier dhparam : `openssl dhparam -out /etc/gitlab/dhparams.pem 4096`
    *   `nginx['custom_nginx_config'] = "include /var/opt/gitlab/nginx/conf/conf.d/*.conf;"` :
        Juste pour avoir un dossier dans lequel ajouter de la
        configuration Nginx sans avoir à modifier la configuration gitlab.
        C'est très pratique, comme on le verra plus loin.
        Ne pas oublier de le créer s'il n'existe pas déjà via un
        `mkdir -p /var/opt/gitlab/nginx/conf/conf.d/`

#### Configuration de Mattermost

La configuration est très similaire à celle que nous venons de faire sur
Gitlab. Dans le même fichier de configuration `/etc/gitlab/gitlab.rb` :

*   `mattermost_external_url 'https://mattermost.example.org'` : l'URL
    de Mattermost (comme pour gitlab, pas de '=' ici)
*   `mattermost['enable'] = true` : pour activer mattermost
*   `mattermost['service_use_ssl'] = true` : httpS, c'est bien ! :-)
*   `mattermost['service_enable_incoming_webhooks'] = true` : pour
    permettre d'envoyer des messages à mattermost depuis gitlab
    (ou n'importe quel autre outil, tant que le message est bien formé)
*   `mattermost['service_allow_cors_from'] = "gitlab.example.org"` :
    pour autoriser les requêtes provenant d'autres domaines.
    Mettre `'*'` pour tout autoriser, laisser vide (`''`) pour désactiver
*   `mattermost['service_enable_commands'] = true` : autoriser les
    utilisateurs à créer des `slash commands` (voir dans les bonus, en bas de cet article)
*   `mattermost['team_site_name'] = "Mon super mattermost"` : Le petit
    nom de votre mattermost. Chez nous, c'est "Framateam"
*   `mattermost['team_enable_team_listing'] = true` : autoriser les
    équipes à se placer (volontairement, en *opt-in*) dans l'annuaire
    des équipes, permettant à quiconque de choisir une équipe existante
    depuis la page d'accueil
*   Pour permettre l'authentification avec les comptes de votre Gitlab :
    *   `mattermost['gitlab_enable'] = true`
    *   `mattermost['gitlab_secret']` et `mattermost['gitlab_id']` :
        pour obtenir le gitlab_secret et le gitlab_id, créez une application
        dans la partie admin de gitlab, avec comme "Redirect URL"
        `https://mattermost.example.org/signup/gitlab/complete` et
        `https://mattermost.example.org/login/gitlab/complete` (une URL par ligne)
    *   `mattermost['gitlab_auth_endpoint'] = "https://gitlab.example.org/oauth/authorize"`
    *   `mattermost['gitlab_token_endpoint'] = "https://gitlab.example.org/oauth/token"`
    *   `mattermost['gitlab_user_api_endpoint'] = "https://gitlab.example.org/api/v3/user"`
*   Et pour configurer le serveur mail (comme pour gitlab) :
    *   `mattermost['email_enable_sign_up_with_email'] = true`
    *   `mattermost['email_enable_sign_in_with_email'] = true`
    *   `mattermost['email_enable_sign_in_with_username'] = true`
    *   `mattermost['email_send_email_notifications'] = true`
    *   `mattermost['email_require_email_verification'] = true`
    *   `mattermost['email_smtp_username'] = "mattermost@example.org"`
    *   `mattermost['email_smtp_password'] = "password"`
    *   `mattermost['email_smtp_server'] = "mail.mattermost.org"`
    *   `mattermost['email_smtp_port'] = "465"`
    *   `mattermost['email_connection_security'] = "TLS"`
    *   `mattermost['email_feedback_name'] = "Notification Mattermost"`
    *   `mattermost['email_feedback_email'] = "no-reply@example.org"`
*   `mattermost['support_email'] =  "support@example.org"` : l'adresse
    e-mail du support
*   `mattermost['privacy_show_email_address'] = false` : les adresses
    e-mail des inscrits ne seront pas affichées
*   `mattermost['privacy_show_full_name'] = false` : pareil pour leur
    vrai nom. Seuls les pseudos seront affichés
*   `mattermost_nginx['enable'] = true` : c'est mieux si votre mattermost
    est accessible par le web ;-)
*   `mattermost_nginx['redirect_http_to_https'] = true` : httpS. Toujours httpS !
*   `mattermost_nginx['ssl_certificate']` : le chemin absolu du certificat
    (avec sa chaîne de certification)
*   `mattermost_nginx['ssl_certificate_key']` : le chemin absolu de la
    clé du certificat
*   `mattermost_nginx['ssl_dhparam']` : même remarque que pour gitlab ci-dessus

Et on termine l'installation avec :

    sudo gitlab-ctl reconfigure

Ouf ! Ça en fait des choses à configurer. :-)

### 3 - Arroser

![](images/icons/arroser.png)

#### Un petit mot à propos de gitlab-ctl

Gitlab vient avec son Nginx, son PostgreSQL, etc. On ne peut pas les
relancer avec `service nginx restart`.

À la place on utilise `gitlab-ctl restart nginx`.
Lancez `gitlab-ctl help` pour voir toutes les options.

<div class="alert alert-info">
  Pensez d'ailleurs à ne pas mettre en concurrence (sur le même port)
  le nginx de gitlab avec un éventuel apache/nginx installé sur votre machine !
  (Votre serviteur s'est retrouvé avec un Gitlab « qui ne marchait pas »
  avec une erreur 404, avant de comprendre que le nginx <em>stand-alone</em>
  était démarré et prenait la requête en premier… ;-) )
</div>

Comme gitlab vient avec son PostgreSQL, pour accéder à la base de données,
il faut faire `su gitlab-psql -s /bin/bash /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql/ gitlabhq_production`
ou plus simplement `gitlab-rails dbconsole` (plus long pour arriver à
la console, mais plus simple à retenir)

#### Mettre à jour Gitlab et Mattermost

Dans la première partie, nous avons récupéré un script qui ajoutait dans
les sources de paquets celui de gitlab. Il est donc très simple de mettre
à jour gitlab, que ce soit avec tous les paquets en même temps
(`sudo apt-get upgrade`) ou ce paquet spécifique
(`sudo apt-get install --only-upgrade gitlab-ce`).

### 4 - Pailler

![](images/icons/pailler.png)

#### IPv6

Parce que gitlab, c'est bien, mais gitlab en IPv6, c'est mieux !
Mais Gitlab ne prend pas en compte IPv6 par défaut.

Pour activer IPv6, il faut éditer la configuration générale de gitlab :

    vi /etc/gitlab/gitlab.rb

Là, décommentez la ligne suivante :

*   `nginx['listen_addresses'] = ['*', '[::]']`

Il reste à appliquer les changements :

    sudo gitlab-ctl reconfigure

Pour tester que ça fonctionne, une méthode posible est d'utiliser `curl` depuis
chez vous, en supposant que votre FAI vous fournit une adresse IPv6 :

    curl -6 https://gitlab.example.org

Si ça vous dit "Could not resolve host", c'est qu'il faut ajouter un enregistrement
IPv6 (`AAAA`) à votre entrée DNS.

Si ça vous dit "Connection refused", c'est que gitlab n'est toujours pas joignable
en IPv6, vérifiez la configuration de gitlab et la configuration d'un éventuel pare-feu.

En revanche, si vous voyez "You are being redirected", c'est que c'est bon !

Il existe également des sites web qui permettent de tester la configuration IPv6 d'un serveur.

#### Synchronisation avec des projets github (Mirroring)

Il suffit d'installer [WebHooker][7], qui vous permettra de faire une
copie miroir de vos projets Gitlab sur votre compte Github.

##### Installation

Le wiki de ce projet étant déjà bien documenté (et en français !),
nous passerons rapidement sur l'installation  :

Dépendances nécessaires  :

    sudo su
    apt-get install git build-essential
    cpan Carton

(Suivre les recommandations par défaut lors de l'installation de carton)

<div class="alert alert-info">
  Attention à ne pas oublier la majuscule à `Carton` lors de la
  commande cpan ! (sinon il ne trouvera pas le package)
</div>

Installation de Web Hooker  :

    sudo su
    cd /opt/
    git clone https://framagit.org/luc/webhooker.git web_hooker
    cd web_hooker
    carton install
    cp web_hooker.conf.template web_hooker.conf

Éditez le fichier `web_hooker.conf` afin qu'il corresponde à votre
configuration : `repo_dir`, `authorized`, et l'adresse d'écoute et user
dans la partie `hyptnotoad` (il est court et bien commenté, c'est assez
simple de s'y retrouver).

Vous devrez y renseigner un utilisateur et mot de passe GitHub pour que
WebHooker puisse pousser les modifications en son nom.

<div class="alert alert-info">
  Mention spéciale pour l'option <code>authorized</code>, en particulier
  si vous comptez ouvrir votre Gitlab à d'autres utilisateurs :
  WebHooker donne le droit à tous les utilisateurs du Gitlab de pousser
  des modifications au nom de l'utilisateur configuré (si ce dernier a
  les droits de le faire bien sûr).
  Ce qui n'est peut-être pas recommandé si vous avez d'autres projets
  sur GitHub que vous souhaiteriez ne pas modifier.
  L'option <code>authorized</code> est là pour ça :
  Il s'agit d'une liste blanche des projets sur lesquels WebHooker peut
  pousser des modifications. Pensez à y jeter un oeil et le configurer !
</div>

Reste ensuite à lancer WebHooker au démarrage (nous utilisons systemd ici,
mais une configuration existe pour initV dans la documentation de WebHooker) :

    sudo cp /opt/web_hooker/utilities/web_hooker.service /etc/systemd/system

Éditer `/etc/systemd/system/web_hooker.service` fraîchement copié pour
l'adapter - en particulier à votre répertoire d'installation dans
`WorkingDirectory` (pour nous, `/opt/web_hooker/`).

N'oubliez pas de vérifier les droits de votre dossier d'installation,
pour que le service puisse le démarrer! (utilisateur `git` par défaut)

Et pour finir:

    sudo su
    systemctl daemon-reload
    systemctl enable web_hooker.service
    systemctl start web_hooker.service

##### Utilisation

C'est installé et en cours d'exécution, tout ce que vous avez à faire
maintenant est d'ajouter dans gitlab un web hook à votre projet avec une
URL de la forme suivante :

    http://127.0.0.1:4242/<github_username>/<github_repo>

![](images/gitlab/framagit-webhookmenu.png)

![](images/gitlab/framagit-webhookfield.png)

#### Recherches sur giphy depuis Mattermost

Il suffit d'installer [Giphy Mat Hooker][8] puis de configurer des
*[slash commands][9]* (ou commandes slash, commencant par le caractère `/`)
dans un chat Mattermost pour chercher et afficher des gifs depuis http://giphy.com/.

##### Installation

Pour les mêmes raisons que webhooker, nous passerons rapidement sur l'installation.
N'hésitez pas à jeter un oeil à la documentation de Giphy Mat Hooker.

Les dépendances sont d'ailleurs pratiquement les mêmes :

    sudo su
    apt-get install git build-essential libssl-dev
    cpan Carton

<div class="alert alert-info">
  Attention à ne pas oublier la majuscule à `Carton` lors de la
  commande cpan ! (sinon il ne trouvera pas le package)
</div>

    cd /var/www/
    git clone https://framagit.org/framasoft/giphymathooker.git
    cd giphymathooker
    carton install
    cp giphy_mat_hooker.conf.template giphy_mat_hooker.conf
    chown -R www-data:www-data /var/www/giphymathooker/

Le fichier de configuration `giphy_mat_hooker.conf` fraîchement copié
contient les explications nécessaires pour que vous l'adaptiez à votre
besoin, comme webhooker (ça se voit tant que ça que le développeur est
le même? ;-) ).
En particulier, jetez un oeil à la clef d'API Giphy si vous souhaitez
un usage en production.

Au besoin, un template de configuration pour nginx existe dans le sous
dossier `utilities` si vous souhaitez mettre Giphy Mat Hooker derrière
un reverse proxy.

Plus qu'à activer le service au démarrage de la machine :

    sudo su
    cp utilities/giphymathooker.service /etc/systemd/system/
    vi /etc/systemd/system/giphymathooker.service
    systemctl daemon-reload
    systemctl enable giphymathooker.service
    systemctl start giphymathooker.service

##### Utilisation dans Mattermost

Jetez tout d'abord un œil à la [documentation Mattermost sur les slash commands][9].

Il faut créer avec un compte administrateur la slash command.
Clic sur le nom d'équipe (coin haut gauche), puis "Integrations" et "Slash commands"

![](images/gitlab/mattermost-integrationsmenu.png)

![](images/gitlab/mattermost-slashcommandsmenu.png)

Créez ensuite votre commande slash en renseignant l'URL de l'installation
(par exemple https://giphy.example.org), puis laissez par défaut la
méthode `POST` (cela n'a pas d'importance, GMH est capable de répondre
aux deux méthodes).
Une fois validé, profitez pour noter le jeton (*token*) attribué par Mattermost.

Retournez dans le fichier de configuration `giphy_mat_hooker.conf` et
renseignez le jeton sus-nommé.
Si vous ne le renseignez pas, n'importe quel service sera capable de
le solliciter s'il fournit les paramètres de Mattermost.

Vous pouvez désormais chercher des gifs sur Giphy avec la commande
`/gif ma recherche` (si vous avez choisi `gif` comme mot clef pour la commande slash).

* * *

*Un **très** grand merci à Framasky pour son aide et ces heures de debug
passées ensemble ! Vous avez ce tutoriel en grande partie grâce à lui* :-)

 [1]: https://about.gitlab.com/
 [2]: https://framagit.org
 [3]: https://about.mattermost.com/
 [4]: https://framateam.org/
 [5]: https://about.gitlab.com/2015/08/18/gitlab-loves-mattermost/
 [6]: http://docs.gitlab.com/ce/incoming_email/README.html
 [7]: https://framagit.org/luc/webhooker
 [8]: https://framagit.org/framasoft/giphymathooker
 [9]: http://docs.mattermost.com/developer/slash-commands.html