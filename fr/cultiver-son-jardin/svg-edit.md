# Installation de SVG-Edit

![](images/svg-edit/svg-edit.png)

[<abbr>SVG</abbr>-<span lang="en">Edit</span>][1] est le logiciel
d’édition d’images vectorielles que nous proposons comme service en
ligne sur [Framavectoriel][2].

![](images/svg-edit/framavectoriel.jpg)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span> Dans la suite de
  ce tutoriel, nous supposerons que vous avez déjà fait pointer votre nom
  de domaine sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
  et que vous disposez d’un espace sur votre serveur pour accueillir ce logiciel.
</p>

## Installation

![](images/icons/bonzai.png)

<abbr>SVG</abbr>-<span lang="en">Edit</span> est comme un bonzaï :
on le pose quelque part et il pousse tout seul.
L’installation sur un serveur est extrêmement simple puisqu’il ne stocke
rien en ligne et n’est constitué que d’HTML, de CSS et de javascript.

Donc, téléchargez les fichiers de la dernière version sur le [site officiel][1]
(pour ce tutoriel il s’agit de la [version 2.7.1][4])

Décompressez l’archive en local et uploadez le tout sur votre serveur.

Pour utiliser le logiciel, il suffit de vous rendre sur
`http://votre-site.org/svg-edit/svg-editor.html`

### Tailler

![](images/icons/tailler.png)

Par défaut, le logiciel est configuré pour sauvegarder automatiquement
en local dans le navigateur (si c’est possible techniquement)
les paramètres personnalisés de l’utilisateur et les images en cours d’édition.
Ce comportement peut avoir des conséquences sur la confidentialité des données
(lorsqu’un profil de navigateur est partagé par différents utilisateurs
dans une salle de classe par exemple), une fenêtre modale s’affiche à
l’ouverture pour permettre à l’utilisateur de choisir quelles données il souhaite mémoriser.

![](images/svg-edit/svg-edit-modale.png)

Comme nous savons que Framavectoriel est très utilisé en milieu scolaire
et dans des contextes divers, nous avons désactiver cette fenêtre modale.

Pour cela, il faut modifier le fichier `config.js` pour ajouter

    noDefaultExtensions : true,
    extensions: [
        'ext-overview_window.js',
        'ext-markers.js',
        'ext-connector.js',
        'ext-eyedropper.js',
        'ext-shapes.js',
        'ext-imagelib.js',
        'ext-grid.js',
        'ext-polygon.js',
        'ext-star.js',
        'ext-panning.js'
    ],
    noStorageOnLoad: true,

juste avant la ligne 11 (`allowedOrigins: [window.location.origin]`) de
manière à désactiver l'extension `ext-storage.js`.

Si vous avez besoin d’améliorer la traduction, le fichier de langue se
trouve dans `locale/lang.fr.js`.

<p class="text-center">
  <span class="fa fa-leaf" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: https://github.com/SVG-Edit/svgedit
 [2]: http://framavectoriel.org/
 [4]: http://svg-edit.googlecode.com/svn/branches/2.7/build/svg-edit-2.7.1.zip
