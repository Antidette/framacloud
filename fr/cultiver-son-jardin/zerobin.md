# Installation de Zerobin

[Zerobin][1] est un logiciel libre qui permet de partager des textes de
manière confidentielle et sécurisée. Il est développé par [Sébastien Sauvage][2].

![](images/zerobin/zerobin.png)

Les données sont chiffrées dans le navigateur web en utilisant
[l’algorithme <attr title="Advanced Encryption Standard">AES</attr> 256 bits][3],
le serveur reçoit les données et l’identifiant du document mais ne
connaît pas la clé qui permet de déchiffrer le texte.
Nous proposons ce logiciel comme service en ligne sur [Framabin][4].
Il est extrêmement simple à installer sur un serveur et peu gourmand en
ressources car il ne requiert aucune base de données.

![](images/zerobin/framabin.jpg)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span> Dans la suite de
  ce tutoriel, nous supposerons que vous avez déjà fait pointer votre
  nom de domaine sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
   et que vous disposez d'un serveur web et de PHP en version supérieure à 5.2.6.
</p>

## Installation

### 1 - Planter

![](images/icons/semer-pot.png)

Téléchargez les fichiers de la dernière version sur le [dépôt Github officiel][5]
(bouton vert « Clone or Download » puis « Download ZIP » à droite ou
bien en ligne de commande `git clone https://github.com/sebsauvage/ZeroBin.git`).
Décompressez l'archive et uploadez le tout sur votre serveur web.
Pour utiliser le logiciel il suffit de vous rendre sur
`http://votre-site.org/ZeroBin-master/`
(vous pouvez évidemment renommer le dossier).
Les dossiers `ZeroBin-master/data` et `ZeroBin-master/tmp` doivent être
accessibles en écriture par l'utilisateur.

### 2 - Tailler et désherber

![](images/icons/tailler.png)

Par défaut, le logiciel est en anglais. Nous l'avons traduit,
l'essentiel du texte utilisé dans l'interface se trouve dans les fichiers
`index.php`, `tpl/page.html` et `js/zerobin.js`.
Pour ceux qui le souhaitent, nous avons mis à disposition la traduction
sur [notre dépôt git][6] (branche « zerobin-fr »).
Il suffit de remplacer le fichier `index-fr.php` par `index.php` pour
obtenir la version francisée.
Pour trouverez également sur le dépôt [notre version personnalisée avec
Bootstrap][7] (branche « master »).

<p class="text-center">
  <span class="fa fa-leaf" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: http://sebsauvage.net/wiki/doku.php?id=php:zerobin
 [2]: http://sebsauvage.net
 [3]: http://fr.wikipedia.org/wiki/Advanced_Encryption_Standard
 [4]: https://framabin.org
 [5]: https://github.com/sebsauvage/ZeroBin
 [6]: https://framagit.org/framasoft/framabin/tree/zerobin-fr/
 [7]: https://framagit.org/framasoft/framabin/tree/master
