# Installation de Wisemapping

![](images/wisemapping/wisemapping-logo.png)

[Wisemapping][1] est le logiciel d'édition collaborative de cartes mentales que nous proposons sur [Framindmap][2].

![](images/wisemapping/framindmap.png)

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p role="alert" class="alert alert-info">
  <span class="label label-primary">Informations</span><br /> Dans ce tutoriel, les instructions seront données pour un serveur dédié sous Debian Jessie avec une base de données MySQL et l’utilisation d’un serveur Nginx comme proxy inverse.<br />Nous supposerons que vous avez déjà fait pointer votre nom de domaine sur votre serveur auprès de votre <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>.
</p>

<p role="alert" class="alert alert-warning">
  <span class="label label-danger">Attention</span><br /> Le logiciel Wisemapping n’a pas évolué depuis octobre 2015 si bien qu’il pose des problèmes de compatibilité avec les dernières version de Java. L'installation est bien plus complexe que ce qu’elle devrait.
</p>

## Prérequis

### Java Development Kit 8

Pour faire fonctionner Wisemapping, il est nécessaire d'installer Java Development Kit 8.

Avec la version 4.0.3 de Wisemapping, il y a une incompatibilité avec les versions d'OpenJDK de Debian Jessie. Soit elle est trop ancienne (OpenJDK 7), soit trop récente (OpenJDK 8u111). La dernière version qui fonctionne est la 8u77.

Il a donc fallu backporter cette version dans Jessie :

<pre>sudo echo "deb-src http://snapshot.debian.org/archive/debian/20160402T041108Z/ sid main" > /etc/apt/sources.list.d/snapshot_openjdk.list
sudo apt-get update -o Acquire::Check-Valid-Until=false
sudo apt-get install dpkg-dev dpatch debhelper build-essential quilt zip sharutils pkg-config wdiff fastjar autoconf automake autotools-dev libtool g++-4.9 libxtst-dev libxi-dev libxaw7-dev libxrender-dev libcups2-dev libasound2-dev liblcms2-dev libfreetype6-dev libgtk2.0-dev libxinerama-dev libkrb5-dev xsltproc libpcsclite-dev libffi-dev zlib1g-dev libattr1-dev libpng-dev libjpeg-dev libgif-dev libpulse-dev systemtap-sdt-dev libnss3-dev mauve jtreg xvfb xfonts-base twm</pre>

Se placer dans un dossier où on peut prendre plus de 10Gio de place et télécharger les sources d'OpenJDK

<pre>apt-get source openjdk-8
cd openjdk-8-8u77-b03</pre>

Modifier `debian/rules` (`nano debian/rules`) et ligne 37, changer `sid` par `jessie` (`distrel := jessie`).

Puis compiler avec `dpkg-buildpackage -rfakeroot -d`. La compilation prend beaucoup de temps, donc c'est le moment de faire autre chose.

Ensuite, si tout se passe bien, on aura des paquets .deb dans le répertoire parent. Il ne reste plus qu'à faire :

<pre>cd ..
dpkg -i openjdk-8-jdk_8u77-b03-3_amd64.deb openjdk-8-jdk-headless_8u77-b03-3_amd64.deb openjdk-8-jre_8u77-b03-3_amd64.deb openjdk-8-jre-headless_8u77-b03-3_amd64.deb</pre>

Et on finit en bloquant la version de ces paquets pour éviter qu'ils ne soient mis à jour :

<pre>echo "openjdk-8-jdk hold" | dpkg --set-selections
echo "openjdk-8-jre hold" | dpkg --set-selections
echo "openjdk-8-jdk-headless hold" | dpkg --set-selections
echo "openjdk-8-jre-headless hold" | dpkg --set-selections</pre>

<p class="fc_m5">
  <i>Merci à <a href="https://twitter.com/teslawf/status/783551810229440513">πcolas Tesla</a> de nous avoir mis sur la piste.</i>
</p>

### Premier pas - Semer en pot

![](images/icons/semer-pot.png)

Ensuite, vous n'avez qu'à télécharger [le fichier .zip sur le site officiel][3], extraire le contenu dans un dossier, ouvrir ce dossier dans un terminal et exécuter la commande `./start.sh` (également possible sur Windows en tapant <code style="white-space:normal">java -Xmx256m -Dorg.apache.jasper.compiler.disablejsr199=true -jar start.jar</code>).

![](images/wisemapping/wisemapping.gif)

Lorsque vous voyez apparaître la ligne <code style="white-space:normal">INFO:oejs.AbstractConnector:Started SelectChannelConnector@0.0.0.0:8080</code>, le serveur web et Wisemapping sont opérationnels.

Pour utiliser Wisemapping, ouvrez votre navigateur web à l'adresse `http://localhost:8080/wisemapping`.
Vous pouvez dès à présent enregistrer un compte (ou utiliser le compte « test@wisemapping.org » / mot de passe « test ») et créer des cartes mentales.

![](images/wisemapping/wisemapping-firefox.png)

Cependant, il s'agit là d'une « installation » minimale du logiciel pour un usage personnel et local.

Par défaut, Wisemapping est pré-configuré pour tourner avec une base de données HSQLDB et avec un serveur web [Jetty][4] qui occupe le port 8080.

Dans le cadre d'une utilisation en ligne multi-utilisateurs, il est nécessaire d'avoir un serveur dédié et de procéder à quelques changements de configuration.

### Base de données

Sur la page d'accueil du logiciel, il y a un message d'avertissement indiquant qu'il est préférable d'utiliser MySQL au lieu de HSQLDB. Il est aussi possible d'utiliser une base de données PostgreSQL pour améliorer les performances mais les développeurs de Wisemapping testent peu cette configuration.

### Reverse proxy et nom de domaine

Pour proposer le service avec un nom de domaine et une URL plus propre il faut utiliser un [proxy inverse][5] via un serveur web comme [Apache][6] ou [Nginx][7],

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d'abord, connectez-vous en tant que `root` sur votre serveur et créez un compte utilisateur `wisemapping` ainsi que le dossier `/var/www/wisemapping` dans lequel seront copiés les fichiers avec les droits d'accès correspondants.

<pre>useradd wisemapping
groupadd wisemapping
mkdir /var/www/wisemapping
chown wisemapping:wisemapping -R /var/www/wisemapping</pre>

### 2 - Semer

![](images/icons/semer.png)

Connectez-vous avec l'utilisateur `wisemapping` : `su wisemapping -s /bin/bash`

Téléchargez le fichier .zip et copiez son contenu dans le dossier `/var/www/wisemapping`

<pre>cd /var/www/wisemapping
wget https://bitbucket.org/wisemapping/wisemapping-open-source/downloads/wisemapping-v4.0.3.zip
unzip wisemapping-v4.0.3.zip
mv wisemapping-v4.0.3/* . && rmdir wisemapping-v4.0.3 && rm wisemapping-v4.0.3.zip
</pre>

### 3 - Arroser

![](images/icons/arroser.png)

#### MySQL

Il faut maintenant créer la base de données et configurer Wisemapping.

Installez tout d'abord le paquet `mysql-server` (notez le mot de passe root) et démarrez MySQL : `service mysql start`

Dans le dossier `config/database/mysql` de wisemapping se trouvent les fichiers .sql permettant de créer la base de données, créer ou mettre à jour la structure des tables et éventuellement les remplir avec des données en exemple.

Modifiez le fichier create-database.sql pour changer le mot de passe (ligne 10) `PASSWORD('ici_le_mot_de_passe')` et si vous le souhaitez vous pouvez aussi changer l'utilisateur (lignes 9 et 10)`'ici_l_utilisateur'@'localhost'`.

On crée la base de données en ligne de commande :

<pre>cd /var/www/framindmap.org/config/database/mysql
mysql -uroot -pmot_de_passe_root_mysql &lt; create-database.sql</pre>

Puis la structure des tables :

<pre>mysql -uroot -pmot_de_passe_root_mysql &lt; create-schemas.sql</pre>

(idem avec `apopulate-schemas.sql` si vous souhaitez ajouter les données en exemple)

#### Wisemapping

Maintenant que la base de données est prête, il faut configurer Wisemapping pour qu'il puisse s'en servir.

Éditez le fichier `webapps/wisemapping/WEB-INF/app.properties`, décommentez (ie enlever les #) les lignes concernant MySQL et commentez celles concernant HSQL.

Remplacez les valeurs de `database.username=` et `database.password=` par l'utilisateur et le mot de passe choisis précédemment dans le fichier `create-database.sql`

#### Email

Toujours dans le fichier `app.properties`, modifiez la partie <em lang="en">Plain SMTP Server Configuration</em> avec les paramètres SMTP d'une adresse à vous qui fonctionne.

Cette étape est indispensable pour que les utilisateurs inscrits puissent recevoir les e-mails de confirmation, les notifications lorsqu'une carte est partagée ou un nouveau mot de passe lorsqu'ils l'ont oublié.

#### Processus silencieux

Lorsque qu'on lance le script `start.sh`, le serveur tourne tant qu'on ne quitte pas le terminal. Pour éviter de devoir conserver un terminal ouvert en permanence, on va ajouter wisemapping en tant que service.

Copiez le fichier `service/wisemapping` dans le dossier `/etc/init.d` et créez les liens symboliques pour activer le service :

<pre>mv /var/www/wisemapping/service/wisemapping /etc/init.d
update-rc.d wisemapping defaults
</pre>

Modifiez ensuite le fichier `/etc/init.d/wisemapping` pour corriger l’emplacement où le logiciel est installé (`WISE_HOME="/opt/wisemapping"` par `WISE_HOME="/var/www/wisemapping"` dans notre exemple)

Wisemapping peut maintenant être démarré, arrêté, relancé avec les commandes usuelles `service wisemapping (start|stop|restart)` et sera lancé automatiquement si jamais la machine doit redémarrer.

<p role="alert" class="alert alert-info">
  <span class="label label-primary">Information</span><br /> Chaque fois que vous effectuez un changement dans le fichier <code>app.properties</code>, il vous faudra relancer wisemapping.
</p>

### 4 - Pailler

![](images/icons/pailler.png)

À ce stade, si tout s'est bien passé, lorsque vous exécutez le script `start.sh`, Wisemapping est pleinement fonctionnel. Vous n'avez qu'à vous rendre sur l'URL `http://ip_de_votre_serveur:8080/wisemapping` pour pouvoir l'utiliser.

Nous allons maintenant configurer Wisemapping pour le rendre accessible depuis un nom de domaine avec Nginx sur une connexion HTTP chiffrée en utilisant un certificat Let's encrypt.

#### Nginx

En tant que `root`, installez le paquet nginx : `apt-get install nginx`

Créez le fichier `/etc/nginx/sites-available/votre-nom-de-domaine` pour y mettre ceci (en remplaçant « votre-nom-de-domaine »). Les textes précédés d’un `#` sont des commentaires pour vous expliquer à quoi servent chaque partie de la configuration. Certaines partie du code sont aussi "commenté" dans le but de les activer plus tard lorsque les certificats auront été créés (cf partie *Let's encrypt* ci-dessous) :

<pre>server {
    listen *:80;           # le site sera accessible sur le port 80, c'est à dire en HTTP, avec l'adresse IPv4
    listen [::]:80;        # … également avec l'adresse IPv6

    #listen *:443 ssl;     # … ainsi qu'en HTTPS (port 443)
    #listen [::]:443 ssl;

    server_name votre-nom-de-domaine votre-nom-de-domaine.org www.votre-nom-de-domaine.org; # … à partir de ces noms de domaine

    access_log /var/log/nginx/votre-nom-de-domaine.access.log; # emplacement où sont enregistrés les logs de visite
    error_log /var/log/nginx/votre-nom-de-domaine.error.log;   # … et d'erreur

    # Certificats SSL
    #ssl_certificate /etc/letsencrypt/live/votre-nom-de-domaine.org/fullchain.pem;
    #ssl_certificate_key /etc/letsencrypt/live/votre-nom-de-domaine.org/privkey.pem;

    #ssl_session_timeout 5m;
    #ssl_session_cache shared:SSL:5m;

    add_header Strict-Transport-Security max-age=15768000; # 6 mois

    # Redirection Proxy vers Wisemapping
    location / {
        include proxy_params;
        proxy_pass  http://127.0.0.1:8080;
    }

    # Redirection Proxy spécifique pour l'accès aux cartes :
    location /c/maps/ {
        # On bloque les moteurs de recherche car les cartes n'ont pas à être rendues publiques
        if ($http_user_agent ~* crawl|Googlebot|Slurp|spider|bingbot|tracker|click|parser|spider|msnbot) {
              return 403;
        }
        include proxy_params;
        # On remplace l'User-Agent pour contourner le blocage des versions récentes de Chrome/Chromium par Wisemapping
        proxy_hide_header User-Agent;
        proxy_set_header User-Agent "Mozilla/5.0 (X11; Linux x86_64; rv:81.0) Gecko/20100101 Firefox/81.0";
        proxy_pass  http://127.0.0.1:8080;
    }

    # On oblige le visiteur à consulter le site en HTTPS
    #if ($server_port = 80) {
    #    rewrite (.*) https://$http_host$1 permanent;
    #}

    # Emplacement réservé à la validation des certificats SSL par certbot
    location ^~ '/.well-known/acme-challenge' {
        default_type "text/plain";
        root /var/www/certbot;
    }
}
</pre>

Activez le site :

<pre>ln -s /etc/nginx/sites-available/votre-nom-de-domaine /etc/nginx/sites-enabled/votre-nom-de-domaine</pre>

Enfin, relancez nginx : `nginx -t && service nginx reload`

#### Let's encrypt

Installez le client `certbot` qui permet de générer les certificats de manière automatisé. Il est disponible dans les dépôts `jessie-backports` :

<pre>echo "deb http://ftp.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list
apt-get update
apt-get install certbot -t jessie-backports</pre>

Dans `/etc/cron.d/certbot` se trouve la tâche planifiée qui renouvellera automatiquement les certificats. Pour que ce renouvellement soit pris en compte par Nginx, ajoutez `--post-hook "/usr/sbin/nginx -s reload"` au bout de la commande (après `certbot -q renew`).

Créez le dossier `/var/www/certbot` puis créez le certificat :

<pre>certbot certonly --rsa-key-size 4096 --webroot -w /var/www/certbot/ --email contact@votre-nom-de-domaine.org --agree-tos -d votre-nom-de-domaine.org -d www.votre-nom-de-domaine.org</pre>

`-d` indique le nom de domaine pour lequel le certificat sera valide. Il faut bien évidemment que les enregistrements DNS de votre registraire soient valides et qu'ils pointent bien vers le serveur où on lance la commande certbot.

`-w` indique la racine du site, là où certbot peut poser ses fichiers pour la vérification.

Les certificats seront disponibles dans `/etc/letsencrypt/live/votre-nom-de-domaine.org` :

*   `cert.pem` : le certificat tout seul
*   `chain.pem` : la chaîne de certification
*   `fullchain.pem` : le certificat concaténé à la chaîne de certification
*   `privkey.pem` : la clé du certificat

Une fois les certificats créés, dé-commentez les lignes `listen […] 443 ssl`, la section « Certificat SSL » et la section « On oblige le visiteur à consulter le site en HTTPS » dans la configuration Nginx puis relancez nginx : `nginx -t && service nginx reload`

<p role="alert" class="alert alert-info">
  <span class="label label-primary">Important</span><br /> Lorsqu'on utilise un proxy inverse, il est important de définir le paramètre <code>site.baseurl</code> dans le fichier <code>app.properties</code>.
</p>

#### Wisemapping à la racine

Pour pouvoir accéder à Wisemapping directement depuis la racine du site, il faut remplacer dans le fichier `contexts/wisemapping.xml` la ligne :

<pre>&lt;Set name="contextPath"&gt;/wisemapping&lt;/Set&gt;</pre>

par

<pre>&lt;Set name="contextPath"&gt;/&lt;/Set&gt;</pre>

### 5 - Tailler et désherber

![](images/icons/tailler.png)

La personnalisation de votre instance de Wisemapping passe par l'édition à la main des fichiers `webapps/wisemapping/jsp/*.jsp`

Ils contiennent le code html des pages d'accueil, de création de compte et de gestions des cartes ainsi que le contenu des fenêtres modales de l'éditeur de cartes pour partager, exporter ou paramétrer son compte.

Par défaut, il y a bien plus de formats proposés à l'export que ce que nous avons mis pour Framindmap : certains sont des formats fermés dont nous ne souhaitons pas encourager l'utilisation (Microsoft Excel, MindManager), d'autres sont défectueux (OpenDocument, images PNG/JPG, PDF).

Certains éléments de l'interface et les e-mails de notification ne sont pas traduits (ou mal). Pour les corriger, il faut modifier les fichiers `messages_fr.properties` (l'interface) et `mail/*.vm` (les mails) qui se trouvent dans l'archive `webapps/wisemapping/lib/wise-webapp-4.0.3.jar`.

En ce qui concerne les mails, certains éléments ne peuvent être corrigé qu'en recompilant le logiciel (fichier `wise-webapp/src/main/java/com/wisemapping/mail/NotificationService.java` dans les sources).

Nous avons donc du recompiler le logiciel pour y apporter ces petites corrections.
Voici donc la [procédure][8]. Elle semble effrayante mais c'est en réalité assez simple et il n’est pas nécessaire de l’effectuer sur le serveur :

*   installer maven (en version 3.1 minimum), sur Ubuntu/Debian `apt-get install maven`
*   importer les sources dans un dossier de travail `git clone https://bitbucket.org/wisemapping/wisemapping-open-source.git`
*   corriger les fichiers problématiques (voici [la listes des fichiers que nous avons du modifier][9])
*   compiler `mvn package -DskipTests` (`-DskipTests` permet d'éviter la phase de vérification des tests unitaires qui échoue du fait de certains bugs non résolus comme certains formats d’exports)
*   à la fin de cette opération, dans le dossier `/wise-webapp/target/wise-webapp-4.0.3/WEB-INF/lib`, se trouve le nouveau fichier `wise-webapp-4.0.3.jar` compilé qu'il suffit de copier dans votre dossier `/var/www/wisemapping/webapps/wisemapping` sur le serveur.

<p class="text-center">
  <span class="fa fa-leaf" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: http://www.wisemapping.com/
 [2]: http://www.framindmap.org
 [3]: https://bitbucket.org/wisemapping/wisemapping-open-source/downloads
 [4]: http://fr.wikipedia.org/wiki/Jetty
 [5]: http://fr.wikipedia.org/wiki/Proxy_inverse
 [6]: http://fr.wikipedia.org/wiki/Apache_HTTP_Server
 [7]: http://fr.wikipedia.org/wiki/Nginx
 [8]: https://bitbucket.org/wisemapping/wisemapping-open-source/src
 [9]: https://framagit.org/framasoft/framindmap/tree/master/_src
