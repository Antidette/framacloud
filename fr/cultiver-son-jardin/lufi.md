# Installation de Lufi

<p class="alert alert-warning">
  <span class="label label-primary">Attention</span> La peinture est fraîche,
  cette page est amenée à évoluer.
  <b>Lufi s'installe de la même manière que <a href="lutim.html">Lutim</a></b>.
  Si vous avez besoin de plus de détail la documentation officielle (en anglais)
  se trouve <a href="https://framagit.org/luc/lufi/wikis/installation">sur le dépôt de Lufi</a>.
</p>

[Lufi][1] est le logiciel d’hébergement de fichier que nous proposons sur [Framadrop][2].

Voici un tutoriel pour vous aider à l’installer sur votre serveur.

<p class="alert alert-info">
  <span class="label label-primary">Informations</span> Dans la suite de
  ce tutoriel, nous supposerons que vous avez déjà fait pointer votre
  nom de domaine sur votre serveur auprès de votre
  <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
  et que vous disposez d’un serveur dédié sous Debian.
</p>

Les instructions données ici sont à effectuer avec l'utilisateur `root`.

## Prérequis

Lufi est codé en Perl, pour le faire fonctionner il est nécessaire d’installer Carton, un gestionnaire de modules Perl.

    apt-get install build-essential libpq-dev
    cpan Carton

## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Tout d’abord, connectez-vous en tant que `root` sur votre serveur et
créez un compte utilisateur `lufi` ainsi que le dossier `/var/www/lufi`
dans lequel seront copiés les fichiers avec les droits d’accès correspondants.

    useradd lufi
    mkdir /var/www/lufi /home/lufi
    chown -R lufi:lufi /var/www/lufi /home/lufi

### 2 - Semer

![](images/icons/semer.png)

Téléchargez les fichiers de la dernière version sur le [dépôt officiel][3]
(« Download zip » en bas à droite ou bien en ligne de commande avec `git`),
copiez son contenu dans le dossier `/var/www/lufi` et attribuez les
droits des fichiers à l’utilisateur `lufi`

    cd /var/www/
    git clone https://git.framasoft.org/luc/lufi.git
    chown lufi:lufi -R /var/www/lufi

Connectez-vous avec l’utilisateur `lufi` : `su lufi -s /bin/bash`
et lancez la commande d’installation des dépendances depuis
le dossier `/var/www/lufi`

    cd /var/www/lufi
    su lufi -s /bin/bash
    carton install
    exit

Maintenant que tout est prêt, modifiez le fichier de configuration
de Lufi `lufi.conf` avec votre éditeur de texte préféré sur le modèle
du fichier `lufi.conf.template`.

Par défaut le logiciel est configuré pour écouter sur le port 8080
de l'adresse 127.0.0.1 (localhost).

    cp lufi.conf.template lufi.conf
    vi lufi.conf

L’ensemble des paramètres sont facultatifs à l'exception du paramètre
`contact` (pensez bien à le configurer et à le décommenter).
Si vous utilisez Lufi derrière un serveur web comme Nginx
(comme dans ce tutoriel), pensez bien à décommenter le paramètre `proxy  => 1,`.

A ce stade de l'installation, il n'est plus nécessaire de se connecter avec
l'utilisateur spécifiquement créé `lufi`. Afin d'améliorer la sécurité, on 
empêchera à présent la possibilité de se connecter à celui-ci avec la commande.

    usermod -s /bin/false lufi

#### Lufi en tant que service

À présent, le serveur tournera lorsque qu’on lancera cette commande :

    carton exec hypnotoad script/lufi

Pour éviter de devoir relancer le serveur à la main à chaque redémarrage
du serveur, on va donc lancer Lufi sous forme de service. Deux possibilités s'offrent alors :

##### Pour InitV

Il faut pour ça copier le script `utilities/lufi.init` dans le fichier
`/etc/init.d/lufi`, le rendre exécutable puis copier le fichier
`utilities/lufi.default` dans `/etc/default/lufi`.

    cp utilities/lufi.init /etc/init.d/lufi
    cp utilities/lufi.default /etc/default/lufi

Il faut maintenant modifier `/etc/default/lufi` pour y mettre le chemin
d’installation de notre Lufi (`/var/www/lufi` si vous n’avez pas changé
le chemin préconisé par ce tutoriel)

    vi /etc/default/lufi
    chmod +x /etc/init.d/lufi
    chown root:root /etc/init.d/lufi /etc/default/lufi

##### Pour Systemd

    cp utilities/lufi.service /etc/systemd/system

Modifiez ce fichier pour qu'il corresponde à votre installation
(User=lufi, WorkingDirectory=/var/www/lufi, …)

    vi /etc/systemd/system/lufi.service

Faites en sorte que Systemd le prenne en compte

    systemctl daemon-reload

Et qu'il le lance à chaque démarrage

    systemctl enable lufi.service

### 4 - Pailler

![](images/icons/pailler.png)

À ce stade, si tout s’est bien passé, lorsque vous exécutez la commande
`service lufi start`, Lufi est pleinement fonctionnel.
Vous n’avez qu’à vous rendre sur l’<abbr>URL</abbr> `http://127.0.0.1:8080` pour pouvoir l’utiliser.

Nous allons maintenant configurer Lufi pour le rendre accessible depuis
un nom de domaine avec Nginx (vous pouvez également utiliser Apache ou
Varnish puisque seule la fonctionnalité de proxy inverse nous intéresse).

#### Nginx

Installez le paquet :

    apt-get install nginx

Créez le fichier de configuration de votre domaine
`/etc/nginx/sites-available/votre-nom-de-domaine` pour y mettre ceci
(en remplaçant « votre-nom-de-domaine ») et le port 8080 si vous
l’avez changé dans la configuration de Lufi :

    server { listen 80;

        server_name votre-nom-de-domaine;

        access_log /var/log/nginx/lufi.success.log;
        error_log /var/log/nginx/lufi.error.log;

        location / {
            # Add cache for static files
            if ($request_uri ~* ^/(img|css|font|js)/) {
                add_header Expires "Thu, 31 Dec 2037 23:55:55 GMT";
                add_header Cache-Control "public, max-age=315360000";
            }
            # HTTPS only header, improves security
            #add_header Strict-Transport-Security "max-age=15768000";

            # Adapt this to your configuration
            proxy_pass  http://127.0.0.1:8080;

            # Really important! Lufi uses WebSocket, it won't work without this
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";

            proxy_http_version 1.1;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

            # If you want to log the remote port of the file senders, you'll need that
            proxy_set_header X-Remote-Port $remote_port;

            proxy_set_header X-Forwarded-Proto $scheme;

            # We expect the downstream servers to redirect to the right hostname, so don't do any rewrites here.
            proxy_redirect     off;
        }
    }

Activez votre fichier :

    ln -s /etc/nginx/sites-available/votre-nom-de-domaine /etc/nginx/sites-enabled/votre-nom-de-domaine

Enfin, relancez nginx :

    service restart nginx

### 5 - Tailler et désherber

![](images/icons/tailler.png)

Pour personnaliser votre instance de Lufi, il faut lancer la commande
`carton exec script/lufi theme votre-theme`, ce qui créera un nouveau
dossier `themes/votre-theme`.
Il contiendra un fichier `Makefile`, un fichier `lib/Lufi/I18N.pm`
ainsi que des dossiers vides pour vous montrer le « squelette » d'un thème.
Ensuite il ne vous reste plus qu'à copier les fichiers du thème `default`
dans votre dossier de thème et à les adapter à votre gré.
Vous pouvez ainsi modifier les fichiers css, images, javascript
(dossier `public`), les modèles de pages (dossier `templates`) et même
la traduction en conservant un thème propre sur lequel basculer en cas de problème.

Le cas de la modification des traductions est un peu particulier :

 *  si vous souhaitez *ajouter* des chaînes de caractères traduites
    dans votre thème, ajoutez-les dans les fichiers du dossier `templates`
    en prenant exemple sur le thème par défaut (`<%= l('Ceci est une chaîne traduite') %>`)
    puis exécutez `make locales` depuis votre dossier de thème (`themes/votre-theme`,
    pas depuis le dossier d'installation de Lufi).
    Il vous restera alors à ajouter la traduction dans les fichiers
    présents dans `themes/votre-theme/lib/Lufi/I18N/`.
 *  si vous souhaitez *modifier* une traduction, repérez la chaîne dans
    les fichiers de `themes/default/lib/Lufi/I18N/`,
    puis recopiez les lignes commençant par `msgid` et `msgstr` qui
    s'y rapportent (ne copiez *surtout pas* les commentaires qui s'y rapportent).
    Enfin, modifiez la traduction (le contenu de `msgstr`) dans vos fichiers.

Modifiez ensuite le fichier `lufi.conf` pour préciser quel thème vous utilisez.
Pour pouvoir personnaliser et observer vos modifications en direct,
il vous faudra stopper temporairement le service `service lufi stop`
et le démarrer avec la commande :

    carton exec morbo script/lufi --listen=http://127.0.0.1:8080

 [1]: https://demo.lufi.io/
 [2]: https://framadrop.org
 [3]: https://framagit.org/luc/lufi