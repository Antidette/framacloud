# Installation de TinyTiny<abbr>RSS</abbr>

**TinyTiny<abbr>RSS</abbr>** (abrégé <abbr>TTRSS</abbr>) est un lecteur de flux
<abbr>RSS</abbr> qui vous permet de suivre l’actualité de vos sites
préférés depuis une seule et même page.

Il s’agit du logiciel que nous proposons pour [Framanews](https://framanews.org)

![](images/ttrss/framanews-screenshot.png)

## 1 - Nourrir la terre

![](images/icons/preparer.png)

<div class="alert alert-info">
  Ce guide reprend les élements présents dans la
  <a href="https://tt-rss.org/wiki/InstallationNotes">page officielle d’installation de <abbr>TTRSS</abbr></a> (en anglais).
</div>

### Pré-requis

Nous allons partir du principe que vous avez accès à un serveur ayant
la configuration suivante ou équivalente :

*   un serveur web et une base de données (<span lang="en">My</span><abbr>SQL</abbr> ou Postgre<abbr>SQL</abbr>),
*   les logins/mots de passe nécessaires à la connexion à la base de données.
    <abbr>TTRSS</abbr> peut coexister avec d’autres applications sur une
    base de données partagée, créer une base de donnée dédiée n’est pas nécessaire,
*   votre serveur web devrait utiliser au moins <abbr>PHP</abbr> 5.3 ou plus récent.
    L’utilisation d’un accélérateur comme `php-apc` est fortement recommandé.

## 2 - Semer

![](images/icons/semer.png)

### Installation

1.  Désarchivez le tar.gz dans le répertoire de destination :

        tar zxfv Tiny-Tiny-RSS-1.x.x.tar.gz

    Il est recommandé de renommer le répertoire en `ttrss` pour plus de commodité :

        mv Tiny-Tiny-RSS-1.x.x ttrss

    Vous pouvez aussi désarchiver le tar.gz sur votre machine locale et
    envoyer les fichiers sur le serveur en utilisant <abbr>FTP</abbr> ou tout autre
    moyen à votre disposition.
-   Vérifiez que vous pouvez accéder à `http://example.com/ttrss/install/`.
-   Procédez à l’installation avec l’assistant.
    Celui-ci vous demandera vos identifiants de base de données et
    l’<abbr>URL</abbr> complète d’accès à votre <abbr>TTRSS</abbr> (par exemple `http://example.com/ttrss/`).
    Cette URL doit si possible être accessible depuis l’extérieur,
    n’utilisez pas localhost, cela casse le support du PUSH.
    Si vous déployez <abbr>TTRSS</abbr> sur un réseau local, vous pouvez vous en passer.
-   L’assistant va générer un fichier `config.php` pour vous une fois que
    vous aurez entré les identifiants de la base de donnée et celle-ci initialisée.
    Vous devrez copier le texte de l’assistant et le copier dans `config.php`
    sur le serveur, dans le répertoire d’installation.
    Si cela lui est possible, l’assistant le fera pour vous de façon automatique.
-   Il est conseillé de relire le fichier `config.php` pour voir si vous
    voulez pas ajouter des fonctionnalités ou changer les valeurs de
    configuration par défaut.
-   Après en avoir fini avec l’assistant, ouvrez votre installation de
    Tiny Tiny <abbr>RSS</abbr> sur `http://example.com/ttrss/` et
    connectez-vous avec les identifiants par défaut (utilisateur : `admin`,
    mot de passe : `password`).
-   Allez dans les préférences et changer votre mot de passe !
-   Vous aurez besoin de choisir la méthode de mise à jour des flux. Voyez plus bas.
-   Si tout s’est bien passé, vous pouvez commencer à utiliser <abbr>TTRSS</abbr>
    normalement. Créez un utilisateur non-administrateur,
    connectez-vous avec celui-ci et commencez à importer vos flux,
    à vous abonner à d’autres et à configurer <abbr>TTRSS</abbr> à votre goût.

### Mise à jour des flux

Vous avez besoin d’utiliser une des méthodes ci-dessous pour utiliser
<abbr>TTRSS</abbr> correctement, sinon vos flux ne seront pas mis à jour.

Utilisez le dæmon de mise à jour si vous pouvez faire tourner des
processus en arrière-plan.
Sinon, utilisez une des autres méthodes.
Sur Debian, les paquets officiels ont un système de mise à jour basé sur `cron`.

#### Dæmon de mise à jour (méthode recommandée)

Utilisez cette méthode si vous avez accès à l’interpréteur <abbr>PHP</abbr> CLI et
pouvez exécuter des processus en arrière-plan.
Vous pouvez lancer un dæmon uni-processus ou `update_daemon2.php`
(multi-processus, lance plusieurs tâches de mise à jour en parallèle)
avec l’interpréteur <abbr>PHP</abbr> CLI.

S’il-vous-plaît, ne lancez jamais le dæmon ou tout autre processus <abbr>PHP</abbr>
en tant qu’utilisateur `root`. Il est recommandé mais non obligatoire de
lancer le dæmon en tant que l’utilisateur de votre site web
(normalement `www-data`, `apache` ou un truc comme ça) pour éviter des
problèmes de propriété de fichier.

Lancez (uni-processus) :

    ./update.php --daemon

ou bien (multi-processus) :

    ./update_daemon2.php

Le script ne se dæmonise pas (c.à.d. ne se détache pas du terminal).
Vous pouvez le forcer à tourner en arrière-plan en utilisant un utilitaire
comme `start-stop-daemon` dans Debian.
Vous pouvez aussi le lancer dans un `screen` ou un `tmux`.
Le dæmon multi-processus *nécessite obligatoirement* que la variable
`PHP_EXECUTABLE` soit correctement configuré dans `config.php` et pointe
vers l’interpréteur <abbr>PHP</abbr> CLI de votre système, sinon il ne
sera pas capable de lancer les tâches de mise à jour.

#### Mise à jour périodique

Utilisez ceci si vous avez accès à l’interpréteur <abbr>PHP</abbr> CLI
mais que vous ne pouvez pas (c.à.d. à cause de votre hébergeur) lancer
de processus d’arrière-plan persistant.
N’essayez pas de lancer des jobs `cron` avec un binaire <abbr>PHP CGI</abbr>,
ça ne fonctionnera pas. Si vous voyez des en-têtes <abbr>HTTP</abbr> quand vous
lancez `php ./update.php`, vous utilisez un mauvais binaire.

Exemple complet (lisez `man 5 crontab` pour plus d’information sur la syntaxe) :

    */30 * * * * /usr/bin/php /home/user/public_html/ttrss/update.php --feeds --quiet

<div class="alert alert-info">
    <ul>
        <li>
            <code>/usr/bin/php</code> doit être remplacé par le chemin
            correct du binaire <abbr>PHP</abbr> CLI sur votre système.
            Si vous n’êtes pas sûr du binaire ou du chemin à utiliser,
            demandez à votre hébergeur.
        </li>
        <li>
            <code>/home/user/public_html/ttrss</code> doit etre remplacé
            par le répertoire dans lequel vous avez installé <abbr>TTRSS</abbr>.
        </li>
        <li>
            Essayez si possible la commande à partir d’un terminal pour
            vérifier que ça fonctionne avant de créer le job cron.
        </li>
    </ul>
</div>

#### Simple mise à jour en arrière-plan (depuis la version 1.7.0)

Si tout le reste a échoué et que vous ne pouvez utiliser une des
méthodes ci-dessus, vous pouvez activer le mode de mise à jour simple
où <abbr>TTRSS</abbr> essayer de mettre à jour périodiquement les flux
tant qu’il est ouvert dans votre navigateur.
Évidemment, il n’y aura pas de mise à jour quand <abbr>TTRSS</abbr> ne
sera pas ouvert ou que votre ordinateur ne sera pas allumé.

Pour activer ce mode, passez la constante `SIMPLE_UPDATE_MODE` à `true`
dans le fichier `config.php`.

Notez bien que seule l’interface principale de <abbr>TTRSS</abbr>
supporte ce mode.
Si vous utilise l’interface `digest` ou `mobile`, ou un client de
l’<abbr>API</abbr> (comme une application Android), les flux ne seront
pas mis à jour.
Vous devez absolument avoir <abbr>TTRSS</abbr> ouvert dans un onglet de
navigateur sur un ordinateur allumé quelque part.

## 3 - Tailler et désherber

![](images/icons/tailler.png)

### Particularités de Framanews

#### Fork

Nous avons forké Tiny Tiny <abbr>RSS</abbr> pour y apporter quelques modifications
nécessaires au bon fonctionnement de Framanews.

*   traduction en français de certains messages qui ne passaient pas
    dans le module de localisation, fuseau horaire Europe/Paris par défaut,
*   augmentation de la durée de rétention du cache des flux,
*   personnalisation du logo, du titre et de la favicon,
*   abonnement automatique au flux du Framablog à l’inscription,
*   utilisation de sous-modules git pour automatiser l’installation de
    plugins que nous voulons proposer,
*   intégration d’un système d’invitation / désabonnement à la liste de
    diffusion des utilisateurs de Framanews lors de l’inscription / la
    suppression du compte,
*   paramétrage de certaines parties du système de mise à jour (plus agressif,
    mais moins gourmand en ressources),
*   création d’une nouvelle méthode (`placesAvailables`) dans
    l’<abbr>API</abbr> pour avoir le nombre de places restantes.

Aucune de ces modifications n’est bloquante : vous pouvez utiliser
notre fork pour votre instance <abbr>TTRSS</abbr>.
Vous pouvez récupérer notre fork sur Framagit (branche "full_french" par défaut).

    git clone https://framagit.org/framasoft/framanews_ttrss.git


#### Plugins

Nous avons aussi développé des plugins <abbr>TTRSS</abbr> pour nos besoins :

##### Quota

Permet de limiter le nombre de flux par utilisateur.
Si l’utilisateur dépasse le nombre de flux autorisé,
il sera redirigé vers la page de configuration avec un message
lui demanant de supprimer les flux surnuméraires.
[Dépôt git](https://framagit.org/framasoft/framanews_quota).

    git clone https://framagit.org/framasoft/framanews_quota.git

##### Framarticle Toolbar

Ajoute un certain nombre de boutons d’accès rapide à l’interface
principale de <abbr>TTRSS</abbr>.
[Dépôt git](https://framagit.org/framasoft/framarticle_toolbar).

    git clone https://framagit.org/framasoft/framarticle_toolbar.git

#### Conseils de Framanews pour une installation de <abbr>TTRSS</abbr> à fort trafic

Nous avons dû faire face à plusieurs problématiques au fur et à mesure
de la mise en place de Framanews.
Voici les leçons que nous en avons tiré :

*   Postgre<abbr>SQL</abbr>. Obligatoire.
    <span lang="en">My</span><abbr>SQL</abbr> ne tient absolument pas
    la charge.
*   Le clustering Postgre<abbr>SQL</abbr> n’est pas encore suffisamment
    économe en ressources pour une utilisation massive de <abbr>TTRSS</abbr>.
    Nous proposons donc plusieurs instances de <abbr>TTRSS</abbr> avec
    chacune sa base de données dédiée.
*   le disque dur peut vite faire un goulot d’étranglement.
    Nous avons pris un hébergeur proposant des machines virtuelles avec
    des disques <abbr>SSD</abbr>.
*   Ne pas hésiter à demander aux utilisateurs qui sont abonnés à plus
    de flux <abbr>RSS</abbr> qu’il n’est humainement possible
    de lire en une journée, de se limiter. Les gens comprennent
    généralement le problème.
    Le plugin de quota permet d’éviter la création d’une situation
    où 20% des utilisateurs possèdent 80% des flux.

<p class="text-center">
  <span class="fa fa-leaf" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>
