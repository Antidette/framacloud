# Installation de Mastodon

Ce tutoriel est en cours de rédaction !

Un peu de patience, promis, nous travaillons dessus ;)

En attendant, vous pouvez consulter [celui-ci qui est déjà très bien][1] en complément de la [documentation officielle en anglais][2].

 [1]: https://angristan.fr/installer-instance-mastodon-debian-8/
 [2]: https://docs.joinmastodon.org/admin/prerequisites/

L'équipe Framasoft.