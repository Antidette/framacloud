# Ajouter une autorité de certification à Debian

Il arrive qu'un serveur distant nous présente une chaîne de certification dont un des maillons est absent de notre chère Debian. Et c'est gênant.

Prenons le cas de Mediapart : un‧e utilisateur‧ice de [Framanews][1] a porté à notre connaissance que le flux RSS de Mediapart lui donnait un vilain message d'erreur, `unable to get local issuer certificate`.

Sachant que Framanews utilise `curl` pour télécharger les flux RSS, il était assez simple de tester dans des conditions similaires avec un simple `curl https://www.mediapart.fr`. Bien évidemment, nous avions la même erreur.

Pour avoir plus de détails, la commande `openssl s_client -connect www.mediapart.fr:443` fut bien utile, car elle nous donnait la chaîne de certification utilisée par le site :

    Certificate chain
     0 s:/OU=Domain Control Validated/OU=Gandi Standard Wildcard SSL/CN=*.mediapart.fr
       i:/C=FR/ST=Paris/L=Paris/O=Gandi/CN=Gandi Standard SSL CA 2
     1 s:/C=FR/ST=Paris/L=Paris/O=Gandi/CN=Gandi Standard SSL CA 2
       i:/C=US/ST=New Jersey/L=Jersey City/O=The USERTRUST Network/CN=USERTrust RSA Certification Authority
     2 s:/C=US/ST=New Jersey/L=Jersey City/O=The USERTRUST Network/CN=USERTrust RSA Certification Authority
       i:/C=SE/O=AddTrust AB/OU=AddTrust External TTP Network/CN=AddTrust External CA Root
     3 s:/C=SE/O=AddTrust AB/OU=AddTrust External TTP Network/CN=AddTrust External CA Root
       i:/C=US/ST=UT/L=Salt Lake City/O=The USERTRUST Network/OU=http://www.usertrust.com/CN=UTN - DATACorp SGC


Après avoir regardé dans `/usr/share/ca-certificates`, il nous est apparu que nous n'avions pas le dernier certificat (`UTN - DATACorp SGC`). Une petite recherche sur le net nous a permis de trouver ce certificat sur <https://www.tbs-certificates.co.uk/FAQ/en/44.html>.

Il ne restait plus qu'à l'ajouter aux certificats de Debian, comme suit :

    mkdir /usr/share/ca-certificates/extra/
    cd /usr/share/ca-certificates/extra/
    vi UTNDataCorpSGC.crt


On met dans ce fichier le certificat trouvé sur <https://www.tbs-certificates.co.uk/FAQ/en/44.html>, puis :

    echo "extra/UTNDataCorpSGC.crt" >> /etc/ca-certificates.conf
    update-ca-certificates


Et voilà ! On vient d'ajouter une nouvelle autorité de certification à Debian, et `curl` arrive enfin à aller sur le site de Mediapart :-)

 [1]: https://framanews.org